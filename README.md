# cmdplay

Navegue nos seus diretórios para encontrar os seus vídeos através do dmenu.

**cmdplay** é um script útil para ser integrados em ambientes mínimos.
A intenção é ligar um atalho do teclado para fazer o dmenu listar um diretório e os vídeos que ele contém, se houver.
Se você selecionar um vídeo ele é executado através do tocador configurado como padrão no arquivo de configuração, ou através do tocador especificado na linha de comando.

# Uso

**cmdplay**: `cmdplay [opções]`\
Exibe, através do 'dmenu' ou 'fzf', uma lista de vídeos para tocar.

**OPÇÕES:**\
`-m, --modo=dmenu|fzf`\
	Indica o modo como a lista de arquivos e diretórios serão exibidas. 'dmenu' mostra em lista suspensana tela. 'fzf' mostra no terminal.\
`-d, --diretorio=/caminho/inicial/onde/estao/os/videos`\
	Caminho do diretório que contém os arquivos que pretente tocar. Os arquivos não precisam estar diretamente dentro desse local, pois, a partir dele, você vai navegar até achar seus vídeos.\
`-t, --tocador=tocador_favorito`\
	Indicar o comando que chama o seu tocador favorito no terminal, exemplo: mpv, gnome-mpv, vlc, mplayer, ffplay, etc...\
`-f, --formatos='formato1 formato2 formato3 ... formatoN'`\
	Lista de formatos que serão filtrados para aparecer na lista do 'dmenu'.\
`-h, --help`\
	Mostra essa lista de ajuda.
	
Você pode se abster de passar todos esses parâmetros na linha de comando,bastando, para isso, deixá-los padronizados no arquivo ~/.config/dmenuplay/config .\
	
**ARQUIVOS:**\
`~/.config/cmdplay/config`

Nesse arquivo pode ser configurado o diretório inicial, o tocador de vídeo preferido e os tipos de vídeos a serem listados. Exemplo:

```
modo modo1
diretório caminho/diretorio/completo
tocador playerXYZ
tipos formato1 formato2
```

Esse arquivo será criado automaticamente, caso não exista, com os seguintes valores:

```
modo dmenu
diretório $HOME
tocador ffplay
formatos mkv mp4 avi mpeg mpg rmvb
```

**PRIORIDADE**

Os argumentos passados na linha de comando tem prioridade sobre as configurações definidas no arquivo ~/.config/dmenuplay/config . Assim, se seu tocador naquele arquivo está definido como X e na linha de comando você passar o argumento -t=Y, então Y, será o seu tocador para essa execução.

# screenshot
![dmenuplay in action](20200611-212323.avi.gif)