#!/usr/bin/env bash
# -------------------------------------------------------------------------
# Script		: cmdplay.sh
# Descrição		: Exibe uma lista de seus diretórios e vídeos para que você escolha um para tocar.
# Versão		: 0.3-beta
# Autor			: Eppur Si Muove
# Contato		: eppur.si.muove@keemail.me
# Criação		: 05/06/2020
# Modificação	: 12/06/2020
# Licença		: GNU/GPL v3.0
# -------------------------------------------------------------------------
# Uso:
#
# -------------------------------------------------------------------------

MENU_AJUDA="
dmenuplay: cmdplay [opções]
	Exibe uma lista de seus diretórios e vídeos para que você escolha um para tocar.

	OPÇÕES:
	-m, --modo=dmenu|fzf
		Indica o modo como a lista de arquivos e diretórios serão exibidas. 'dmenu' mostra em lista suspensana tela. 'fzf' mostra no terminal.

	-d, --diretorio=/caminho/completo/onde/estao/os/videos
		Caminho do diretório que contém os arquivos que pretente tocar. Os arquivos não precisam estar diretamente dentro desse local, pois, a partir dele, você vai navegar até achar seus vídeos.

	-t, --tocador=tocador_favorito
		Indicar o comando que chama o seu tocador favorito no terminal, exemplo: mpv, gnome-mpv, vlc, mplayer, ffplay, etc...

	-f, --formatos='formato1 formato2 formato3 ... formatoN'
		Lista de formatos que serão filtrados para aparecer na lista do 'dmenu'.

	-h, --help
		Mostra essa lista de ajuda.

	Você pode se abster de passar todos esses parâmetros na linha de comando,bastando, para isso, deixá-los padronizados no arquivo ~/.config/dmenuplay/config .

	ARQUIVOS:
	~/.config/cmdplay/config
		Nesse arquivo pode ser configurado o diretório inicial, o tocador de vídeo preferido e os tipos de vídeos a serem listados. Exemplo:

			modo modo1
			diretório caminho/diretorio/completo
			tocador playerXYZ
			tipos formato1 formato2

		Esse arquivo será criado automaticamente, caso não exista, com os seguintes valores:

			modo dmenu
			diretório $HOME
			tocador ffplay
			formatos mkv mp4 avi mpeg mpg rmvb

	PRIORIDADE
	Os argumentos passados na linha de comando tem prioridade sobre as configurações definidas no arquivo ~/.config/cmdplay/config . Assim, se seu tocador naquele arquivo está definido como X e na linha de comando você passar o argumento -t=Y, então Y, será o seu tocador para essa execução.

Relate erros para eppur.si.muove@keemail.me .
"
checa_config(){
	# Verifica se diretório de arquivo de configuração existe
	local cfg_dir="$HOME/.config/cmdplay"
	[[ ! -d $cfg_dir ]] && mkdir -p $cfg_dir

	# Verifica se arquivo de configuração existe
	CFG_ARQ="$cfg_dir/config"
	if [[ ! -f $CFG_ARQ ]]; then
		local cfg_inicial="modo dmenu\ndiretório $HOME\ntocador ffplay\nformatos mkv mp4 avi mpeg mpg rmvb"
		echo -e "$cfg_inicial" > $CFG_ARQ
	fi
}

# Armazena os valores de DIR_INICIAL, TOCADOR e FORMATOS
carrega_variaveis(){
	if [[ $# -gt 0 ]]; then
		for arg in "$@"; do
			local valor=$(echo "$arg" | cut -d= -f2)
			case "$arg" in
				'-m='|'--modo=') echo "Erro: valor de $arg está vazio" && exit 1 ;;
				-m=*|--modo=*) MODO="$valor" ;;
				'-d='|'--diretorio=') echo "Erro: valor de $arg está vazio" && exit 1 ;;
				-d=*|--diretorio=*) DIR_INICIAL="$valor" ;;
				'-t='|'--tocador=') "Erro: valor de $arg está vazio" && exit 1 ;;
				-t=*|--tocador=*) TOCADOR="$valor" ;;
				'-f='|'--formatos=') "Erro: valor de $arg está vazio" && exit 1;;
				-f=*|--formatos=*) local exts="$valor" ;;
				-h|--help) echo "$MENU_AJUDA" | less -x4 && exit 0 ;;
				*) echo "$arg : opção inválida." && exit 1
			esac
		done
	fi
	[[ -z "$DIR_INICIAL" ]] && DIR_INICIAL=$(grep diretório $CFG_ARQ | cut -d' ' -f2-)
	[[ -z "$TOCADOR" ]] && TOCADOR=$(grep tocador $CFG_ARQ | cut -d' ' -f2)
	[[ -z "$FORMATOS" ]] && FORMATOS=$(grep formatos $CFG_ARQ | cut -d' ' -f2-)
	[[ -z "$MODO" ]] && MODO=$(grep modo $CFG_ARQ | cut -d' ' -f2)
	case $MODO in
		dmenu) MODO="dmenu -i -l 15 -p " ;;
		fzf) MODO="fzf --height=60% --layout=reverse-list --border --margin=5%,0,5% --prompt="
	esac
}

monta_lista(){
	declare -a arqs=("..")
	local raiz="$1"
	[[ -z "$raiz" ]] && raiz="$DIR_INICIAL"
	for dir in "$raiz"/*; do
		[[ -d "$dir" ]] && arqs=("${arqs[@]}" "$dir")
	done
	for arq in "$raiz"/*; do
		for ext in $FORMATOS; do
			[[ $arq == *.$ext ]] && arqs=("${arqs[@]}" "$arq")
		done
	done

	ARQUIVO=$(
		for item in "${arqs[@]}"; do
			echo "${item##*/}"
		done | $MODO"$raiz:» ")
	
	[[ -z "$ARQUIVO" ]] && exit 0

	ARQUIVO="$raiz/$ARQUIVO"

	if [[ "$ARQUIVO" == "$raiz/.." ]]; then
		[[ -z "${ARQUIVO%/*/*}" ]] && monta_lista / ||	monta_lista "${ARQUIVO%/*/*}"
	elif [[ -d "$ARQUIVO" ]]; then
		monta_lista "$ARQUIVO"
	else
		$TOCADOR "$ARQUIVO" &
	fi
}

checa_config
carrega_variaveis "$@"
monta_lista
